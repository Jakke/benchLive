#!/bin/bash
fastq_file=$1
reference_file=$2
run_dir=$3
logfile=$4

errorOccured=0
for file in ${run_dir}/samFiles/L001/s_1_*.sam; do samtools view -bS $file > ${file%.sam}.bam; done ;
[[ $? = 1 ]] && errorOccured=1

[[ $errorOccured = 0 ]] && samtools cat ${run_dir}/samFiles/L001/s_1_*.bam > ${run_dir}/samFiles/finalSamFile.bam || for bamFile in ${run_dir}/samFiles/L001/s_1_1101.1.bam ${run_dir}/samFiles/L001/s_1_1101.bam ; do [[ -f $bamFile ]] && cp $bamFile ${run_dir}/samFiles/finalSamFile.bam ; done 
[[ $? = 1 ]] && errorOccured=1

[[ $errorOccured = 0 ]] && for samFile in ${run_dir}/samFiles/L001/s_1_1101.1.sam ${run_dir}/samFiles/L001/s_1_1101.sam ; do [[ -f $samFile ]] && samtools view -SH $samFile > ${run_dir}/samFiles/finalSamFile.sam ; done 
[[ $? = 1 ]] && errorOccured=1

[[ $errorOccured = 0 ]] && samtools view ${run_dir}/samFiles/finalSamFile.bam >> ${run_dir}/samFiles/finalSamFile.sam
[[ $? = 1 ]] && errorOccured=1

[[ $errorOccured = 0 ]] && echo 'PASSED'
