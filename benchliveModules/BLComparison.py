#!/usr/bin/python3

import os
import re
import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import matplotlib.gridspec as gridspec
from benchliveModules.BLPrinter import *

class BLComparison(object):
  """ A class for storing the information of several runs and generating a summary from it """

  def __init__(self):
    self.names = list()
    self.sensitivityCounts = list() # a list (run1, run2, ...) of dictionaries (ist vs soll) of dictionaries (0-error-counts, 1-error-count, ...)
    self.sensitivities = list() # a list (run1, run2, ...) of lists (1,1,1,0.99,0.76,0,0)
    self.avgSeedCounts = list()

    self.runtimes = list()
    self.diskUsages = list() # in GiByte
    self.memoryUsages = list() # in GiByte

    self.index_runtimes = list()
    self.index_diskUsages = list() # in GiByte
    self.index_memoryUsages = list() # in GiByte


  def addRun(self, runInstance):
    if not runInstance.rabemaPassed():
      BLPrint(runInstance.configuration.runDir + " did NOT finish properly", error=True)
    self.addNames(runInstance)
    self.addSensitivityCounts(runInstance)
    self.addAvgSeedCounts(runInstance)

    self.addRuntimes(runInstance)
    self.addDiskUsages(runInstance)
    self.addMemoryUsages(runInstance)

    self.addIndex_runtimes(runInstance)
    self.addIndex_diskUsages(runInstance)
    self.addIndex_memoryUsages(runInstance)

    s = list()
    for i in range(0,9):
      if self.sensitivityCounts[-1]["soll"][i] > 0:
        s.append(self.sensitivityCounts[-1]["ist"][i] / self.sensitivityCounts[-1]["soll"][i])
      else:
        s.append(float('nan'))
    self.sensitivities.append(s)


  def addNames(self, runInstance):
    self.names.append(os.path.split(runInstance.configuration.runDir)[1])


  def addSensitivityCounts(self, runInstance):
    if os.path.exists(runInstance.configuration.rabemaLogRegular):
      self.sensitivityCounts.append({"soll":dict(), "ist":dict()})

      pattern = re.compile("Intervals to find:")
      firstLineOfSensitivity = 0
      with open(runInstance.configuration.rabemaLogRegular, "r") as f:
        lines = f.readlines()
      for i, line in enumerate(lines):
        m = re.search(pattern, line)
        if m:
          firstLineOfSensitivity = i+20
          break

      pattern = re.compile("^\s*\d+\s+(\d+)\s+(\d+)")
      m = re.search(pattern, lines[firstLineOfSensitivity + 0])
      self.sensitivityCounts[-1]["soll"][0] = int(m.group(1))
      self.sensitivityCounts[-1]["ist"] [0] = int(m.group(2))
      m = re.search(pattern, lines[firstLineOfSensitivity + 1])
      self.sensitivityCounts[-1]["soll"][1] = int(m.group(1))
      self.sensitivityCounts[-1]["ist"] [1] = int(m.group(2))
      m = re.search(pattern, lines[firstLineOfSensitivity + 2])
      self.sensitivityCounts[-1]["soll"][2] = int(m.group(1))
      self.sensitivityCounts[-1]["ist"] [2] = int(m.group(2))
      m = re.search(pattern, lines[firstLineOfSensitivity + 3])
      self.sensitivityCounts[-1]["soll"][3] = int(m.group(1))
      self.sensitivityCounts[-1]["ist"] [3] = int(m.group(2))
      m = re.search(pattern, lines[firstLineOfSensitivity + 4])
      self.sensitivityCounts[-1]["soll"][4] = int(m.group(1))
      self.sensitivityCounts[-1]["ist"] [4] = int(m.group(2))
      m = re.search(pattern, lines[firstLineOfSensitivity + 5])
      self.sensitivityCounts[-1]["soll"][5] = int(m.group(1))
      self.sensitivityCounts[-1]["ist"] [5] = int(m.group(2))
      m = re.search(pattern, lines[firstLineOfSensitivity + 6])
      self.sensitivityCounts[-1]["soll"][6] = int(m.group(1))
      self.sensitivityCounts[-1]["ist"] [6] = int(m.group(2))
      m = re.search(pattern, lines[firstLineOfSensitivity + 7])
      self.sensitivityCounts[-1]["soll"][7] = int(m.group(1))
      self.sensitivityCounts[-1]["ist"] [7] = int(m.group(2))
      m = re.search(pattern, lines[firstLineOfSensitivity + 8])
      self.sensitivityCounts[-1]["soll"][8] = int(m.group(1))
      self.sensitivityCounts[-1]["ist"] [8] = int(m.group(2))
    else:
      self.sensitivityCounts.append(float('nan'))



  def addAvgSeedCounts(self, runInstance):
    pattern_seeds = "Found (\S+) seeds"
    summedSeeds = 0
    for line in runInstance.str_hilive_log.split("\n"):
      m = re.search(pattern_seeds, line)
      if m:
        summedSeeds += int(m.group(1))
    self.avgSeedCounts.append(summedSeeds / runInstance.configuration.readLength)

  def addRuntimes(self, runInstance):
    pattern_runtime = "Total run time: (\d*) s"
    self.runtimes.append(int(re.search(pattern_runtime, runInstance.str_hilive_log).group(1)))
    # m, s = divmod(seconds, 60)
    # h, m = divmod(m, 60)
    # self.timeConsumptionRun = "%d:%02d:%02d" % (h, m, s)


  def usageParser(self, filename):
    with open(filename, "r") as f:
      text = f.read().strip().replace(',','.')
    m = re.match("^(\d+(?:\.\d+)?)(Gi|Mi|Ki)?B$", text)
    if not m:
      BLPrint("Unexpected content of " + filename, error = True)

    if m.group(2)=='Gi':
      return(float(m.group(1)))
    elif m.group(2)=='Mi':
      return(float(m.group(1))/1024)
    elif m.group(2)=='Ki':
      return(float(m.group(1))/1024/1024)
    elif m.group(2)=='':
      return(float(m.group(1))/1024/1024/1024)


  def addDiskUsages(self, runInstance):
    self.diskUsages.append(self.usageParser(runInstance.configuration.diskUsageFile))


  def addMemoryUsages(self, runInstance):
    self.memoryUsages.append(self.usageParser(runInstance.configuration.memoryUsageFile))


  def addIndex_runtimes(self, runInstance):
    if os.path.exists(runInstance.configuration.indexLogFile):
      pattern = "Index creation took (\d*(?:\.\d+)?) seconds and (None|\d*(?:\.\d+)?) GiB of memory"
      with open(runInstance.configuration.indexLogFile, "r") as f:
        indexLog = f.read().strip()
      m = re.search(pattern, indexLog)
      if m:
        try:
          self.index_runtimes.append(int(m.group(1)))
          return
        except ValueError:
          pass
    self.index_runtimes.append(float('nan')) 


  def addIndex_diskUsages(self, runInstance):
    if runInstance.configuration.setting_isSeqanIndex:
      command =  "du -c " + runInstance.configuration.indexFile + "*" 
      command += " --exclude " + runInstance.configuration.indexFile
      command += " --exclude " + runInstance.configuration.indexLogFile
      command += " | tail -n1 | sed s/total$//"
    else:
      command = "du -c " + runInstance.configuration.indexFile + " | tail -n1 | sed s/total$//"
    indexSize = runInstance.__terminal__.sendCommand(command, returnOutput = True)[0]
    indexSize = int(indexSize.strip())/1024/1024
    self.index_diskUsages.append(indexSize)


  def addIndex_memoryUsages(self, runInstance):
    if os.path.exists(runInstance.configuration.indexLogFile):
      pattern = "Index creation took (\d*(?:\.\d+)?) seconds and (None|\d*(?:\.\d+)?) GiB of memory"
      with open(runInstance.configuration.indexLogFile, "r") as f:
        indexLog = f.read().strip()
      m = re.search(pattern, indexLog)
      if m:
        try:
          self.index_memoryUsages.append(float(m.group(2)))
          return
        except ValueError:
          pass
    self.index_memoryUsages.append(float('nan'))


  def getNumberOfRuns(self):
    return(len(self.runtimes))



########################################################################################################################
########################################################################################################################
########################################################################################################################


  def createGraphic(self, outputFile):
    fig = plt.figure(figsize=(15, 15)) # create figure window

    gsMain = gridspec.GridSpec(100, 100) # for spacing
    gs =  gridspec.GridSpecFromSubplotSpec(16, 16, subplot_spec=gsMain[:, :], wspace=0.2, hspace=1.0) # for actual plots
    # gs1 = gridspec.GridSpecFromSubplotSpec(2, 2, subplot_spec=gs[0:4, 4:8], wspace=0.05, hspace=0.35)
    # gs2 = gridspec.GridSpecFromSubplotSpec(2, 2, subplot_spec=gs[4:8, 0:4], wspace=0.05, hspace=0.35)

    ax1      = plt.subplot(gs[0:4,0:6])
    # ax2      = plt.subplot(gs[5:7,0:6])
    axbar1_1 = plt.subplot(gs[0:2,8:10])
    axbar1_2 = plt.subplot(gs[0:2,10:12])
    axbar1_3 = plt.subplot(gs[2:4,8:10])
    axbar1_4 = plt.subplot(gs[2:4,10:12])
    axbar2_1 = plt.subplot(gs[7:9,0:2])
    axbar2_3 = plt.subplot(gs[7:9,3:5])
    axbar2_4 = plt.subplot(gs[7:9,5:7])

    fig.patches.extend([plt.Rectangle((0.06,7/16),0.5,0.11, fill=True, color='white', alpha=0.7, zorder=1000, transform=fig.transFigure, figure=fig)])
    myColorMap = plt.get_cmap('Set1')
    myColors = myColorMap(np.linspace(0, 1, 8))

    # filling upper plot
    for i in range(0,self.getNumberOfRuns()):
      ax1.plot(range(0,9), self.sensitivities[i], linewidth=np.linspace(5,1,self.getNumberOfRuns())[i], dashes=(5+i,4+i), color=myColors[i])
    ax1.grid(True)
    ax1.set_ylim([0,1])
    ax1.set_xlim([0,8])
    ax1.set_ylabel('sensitivity')
    ax1.set_xlabel('errors in alignment')

    # ax2.axis('off')
    boundingBox = [-2.9, # x0
                    2.7 - 0.15*self.getNumberOfRuns(), # y0
                    3.53, # width
                    0.15*self.getNumberOfRuns() ] # height
    cellContent = []
    for vec in self.sensitivities:
      cellContent.append([])
      for elem in vec:
        if math.isnan(elem):
          cellContent[-1].append('')
        elif elem == 0:
          cellContent[-1].append(0)
        else:
          cellContent[-1].append(round(elem, 3))

    table = plt.table(cellText = cellContent, loc = 'top', bbox = boundingBox, cellLoc = 'center', fontsize = 120)
    table.auto_set_font_size(False) 
    table.set_fontsize(6)
    # table_props = table.properties()
    # table_cells = table_props['child_artists']
    # for index, cell in enumerate(table_cells): 
      # cell.set_linewidth(0)
    for rowIndex in range(0,self.getNumberOfRuns()):
      for columnIndex in range(0,9):
        table._cells[(rowIndex, columnIndex)].set_linewidth(0)
        table._cells[(rowIndex, columnIndex)]._text.set_color(myColors[rowIndex])
      # cell._text.set_color(myColors[index % self.getNumberOfRuns()])
      # cell._text.set_fontsize(20)

    # filling lower plot
    # fillLowerPlots([axbar1_1, axbar1_2, axbar1_3, axbar1_4, axbar2_1, axbar2_2, axbar2_3, axbar2_4, axbar3_1, axbar3_2, axbar3_3, axbar3_4], values1_1, values1_2, values1_3, values1_4, values2_1, values2_2, values2_3, values2_4, values3_1, values3_2, values3_3, values3_4)

    def smartCeil(number):
      number = float(number)
      firstRelevantNachkommastelle = -math.floor(math.log10(number))
      firstTwoDigits = float(math.floor(number*(10**(firstRelevantNachkommastelle+1))))
      if firstTwoDigits < 10:
        BLPrint("Unspecified error 1 in smartCeil()", error = True)
      if firstTwoDigits <= 14:
        result = 15 / (10**(firstRelevantNachkommastelle+1))
      elif firstTwoDigits <= 19:
        result = 20 / (10**(firstRelevantNachkommastelle+1))
      elif firstTwoDigits <= 24:
        result = 25 / (10**(firstRelevantNachkommastelle+1))
      elif firstTwoDigits <= 29:
        result = 30 / (10**(firstRelevantNachkommastelle+1))
      elif firstTwoDigits <= 34:
        result = 35 / (10**(firstRelevantNachkommastelle+1))
      else:
        result = math.ceil(firstTwoDigits/10) / (10**firstRelevantNachkommastelle+0)
      return(result)

    axes = [axbar1_1, axbar1_2, axbar1_3, axbar1_4, axbar2_1, axbar2_3, axbar2_4]

    for ax in axes:
        ax.tick_params(
        axis='both',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom='off',      # ticks along the bottom edge are off
        top='off',         # ticks along the top edge are off
        left='off',         # ticks along the top edge are off
        right='off',         # ticks along the top edge are off
        labelbottom='off',
        labelright='off',
        labelleft='off') # labels along the bottom edge are off

        ax.set_xlim([-0.5,self.getNumberOfRuns()-0.5])

    values = [self.runtimes, [float(count)/1000000 for count in self.avgSeedCounts], self.memoryUsages, self.diskUsages]
    values += [self.index_runtimes, self.index_memoryUsages, self.index_diskUsages]
    for i in range(0,7):
      axes[i].bar(range(0,self.getNumberOfRuns()), values[i], align='center', width=0.6, color=myColors)

    axes[0].set_xlabel('runtime')
    axes[1].set_xlabel('avg seed count')
    axes[2].set_xlabel('RAM')
    axes[3].set_xlabel('disk space')
    axes[4].set_xlabel('runtime')
    axes[5].set_xlabel('RAM')
    axes[6].set_xlabel('disk space')

    axes[0].tick_params(left='on', labelleft='on')
    axes[2].tick_params(left='on', labelleft='on')
    axes[4].tick_params(left='on', labelleft='on')
    axes[5].tick_params(left='on', labelleft='on')

    axes[1].tick_params(right='on', labelright='on')
    axes[3].tick_params(right='on', labelright='on')
    axes[6].tick_params(right='on', labelright='on')

    axes[1].yaxis.set_label_position("right")
    axes[3].yaxis.set_label_position("right")
    axes[6].yaxis.set_label_position("right")

    axes[0].set_ylabel('runtime  [s]')
    axes[1].set_ylabel('count  [millions]')
    axes[2].set_ylabel('RAM  [GB]')
    axes[3].set_ylabel('disk space  [GB]')
    axes[4].set_ylabel('runtime  [s]')
    axes[5].set_ylabel('RAM  [GB]')
    axes[6].set_ylabel('disk space  [GB]')

    axes[0].set_ylim([0, smartCeil(max(self.runtimes           )*1.2)])
    axes[1].set_ylim([0, smartCeil(max(self.avgSeedCounts      )*1.2/1000000)])
    axes[2].set_ylim([0, smartCeil(max(self.memoryUsages       )*1.2)])
    axes[3].set_ylim([0, smartCeil(max(self.diskUsages         )*1.2)])
    axes[4].set_ylim([0, smartCeil(max(self.index_runtimes     )*1.2)])
    axes[5].set_ylim([0, smartCeil(max(self.index_memoryUsages )*1.2)])
    axes[6].set_ylim([0, smartCeil(max(self.index_diskUsages   )*1.2)])

    for ax in axes:
      rects = ax.patches
      for rect in rects:
        height = rect.get_height()
        if not math.isnan(height):
          value = 0
          if height != 0:
            if height < 1:
              value = round(height, -math.floor(math.log10(height))+1)
            elif height < 100:
              value = round(height, 2)
            else:
              value = round(height)
          ax.text(rect.get_x() + rect.get_width()/2, height, value, ha='center', va='bottom', size=6)

    for ax in axes:
      plt.setp(ax.get_yticklabels()[-1], visible=False)

    # finalize figure
    fig.add_subplot(ax1)
    for ax in axes:
      fig.add_subplot(ax)

    # bwa_patch = mpatches.Patch   (edgecolor='black', facecolor='red')
    # col01_patch = mpatches.Patch  (edgecolor='black', facecolor=col01)
    # col03_patch = mpatches.Patch  (edgecolor='black', facecolor=col03)
    # col03g_patch = mpatches.Patch (edgecolor='black', facecolor=col03g)
    # col03qr_patch = mpatches.Patch(edgecolor='black', facecolor=col03qr)
    # handles = [col01_patch, col03_patch, col03g_patch, col03qr_patch, bwa_patch]
    # labels = ['HiLive 0.1', 'HiLive 0.3', 'HiLive 0.3gapNaive', 'HiLive 0.3gapQRSeed', 'BWA']

    handles = [ mpatches.Patch(edgecolor = 'black', facecolor = color) for color in myColors ]
    plt.legend(handles, self.names, bbox_to_anchor=(0.45, 0.675), loc = 2, bbox_transform=plt.gcf().transFigure, fontsize='8')

    # save graphic
    if not os.path.exists(os.path.dirname(outputFile)):
      os.makedirs(os.path.dirname(outputFile))
    fig.savefig(outputFile + ".pdf", bbox_inches='tight', dpi=300)
    BLPrint("Graphic saved to " + outputFile + ".pdf")
