#!/usr/bin/python3

import sys
import os
import re
import time
from benchliveModules.BLTerminal import *
from benchliveModules.BLPrinter import *

class BLExecutableHandler(object):
  """ A class for generating and storing HiLive Executables """

  def __init__(self, hilive_build_dir=None, hilive_build_call=None, git_checkout_dir=None):
    self.storageDir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), "hiliveExecutables")
    if hilive_build_dir != None:
      self.git_checkout_dir = os.path.realpath(git_checkout_dir)
      self.hilive_build_dir = os.path.realpath(hilive_build_dir)
      self.hilive_build_call = hilive_build_call


  def storeCommit(self, commit):
    if not os.path.exists(self.storageDir):
      os.makedirs(self.storageDir)
    if os.path.exists(os.path.join(self.storageDir, commit + "_hilive")) and os.path.exists(os.path.join(self.storageDir, commit + "_hilive-build")):
      BLPrint("executables already exist in " + self.storageDir, error=True)
    call1 = "cd " + self.git_checkout_dir
    call2 = "git checkout " + commit
    call3 = "cd " + self.hilive_build_dir
    call4 = self.hilive_build_call
    call5 = "cp " + os.path.join(self.hilive_build_dir, "hilive") + " " + os.path.join(self.storageDir, commit + "_hilive")
    call6 = "cp " + os.path.join(self.hilive_build_dir, "hilive-build") + " " + os.path.join(self.storageDir, commit + "_hilive-build")
    calls = [call1, call2, call3, call4, call5, call6]

    term = BLTerminal()
    __BLCOLOR_ERROR     = '\x1b[1;31m'
    __BLCOLOR_END       = '\x1b[0m'

    for call in calls:
      allowedErrors = ["HEAD is now at .*\n", "Previous HEAD position was .*\n", "Already on '.*'\n"]
      BLPrint(call, attention = True)
      reg, err = term.sendCommand(call, returnOutput = True)
      if reg.strip() != "":
        BLPrint(reg.strip())
      filteredErr = err
      for regex in allowedErrors:
        filteredErr = re.sub(regex, "", filteredErr)
      filteredErr = filteredErr.strip()
      if filteredErr != "":
        BLPrint(err.strip(), error = True)
      if err.strip() != "":
        BLPrint(__BLCOLOR_ERROR + err.strip() + __BLCOLOR_END)


  def getExecutablePathHilive(self, exePrefix):
    path = os.path.join(self.storageDir, exePrefix + "_hilive")
    if not os.path.exists(path):
      BLPrint("Could not find hilive executable " + path + "\nMaybe you need to create it via \x1b[1;33mbenchlive.py store\x1b[0m", error = True)
    return path


  def getExecutablePathHiliveBuild(self, exePrefix):
    path = os.path.join(self.storageDir, exePrefix + "_hilive-build")
    if not os.path.exists(path):
      BLPrint("Could not find hilive-build executable " + path + "\nMaybe you need to create it via \x1b[1;33mbenchlive.py store\x1b[0m", error = True)
    return path
