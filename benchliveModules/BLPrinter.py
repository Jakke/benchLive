import sys
import time

class BenchLiveError(Exception):
  pass

def __printHelper__(text, error, warning, debug, attention):
  __BLCOLOR_END       = '\x1b[0m'
  __BLCOLOR_ERROR     = '\x1b[1;31m'
  __BLCOLOR_WARNING   = '\x1b[0;31m'
  __BLCOLOR_ATTENTION = '\x1b[1;33m'
  __BLCOLOR_DEBUG     = '\x1b[1;37m'
  __BLCOLOR_GREEN     = '\x1b[1;32m'

  
  text = str(text)
  temp = text
  if error:
    temp = __BLCOLOR_ERROR + "ERROR: " + text + "\nEXITING ..." + __BLCOLOR_END
  if warning:
    temp = __BLCOLOR_WARNING + "WARNING: " + text + __BLCOLOR_END
  if debug:
    temp = __BLCOLOR_DEBUG + "DEBUGGING: " + text + __BLCOLOR_END
  if attention:
    temp = __BLCOLOR_ATTENTION + text + __BLCOLOR_END

  temp = time.strftime("[%d/%m/%Y %H:%M.%S]\t") + temp + '\n'
  return(temp)


def BLPrint(text, error=False, warning=False, debug=False, attention=False):
  temp = __printHelper__(text, error, warning, debug, attention)
  if not error:
    sys.stdout.write(temp)
  else:
    sys.stdout.write("\n" + temp)
    sys.exit(1)


class BLPrinter(object):

  def __init__(self, muteBLOutput = False):
    self.muteBLOutput = muteBLOutput
    self.benchliveLog = None

  def setBenchliveLog(self, benchliveLog):
    self.benchliveLog = benchliveLog

  # note, that this function does not exit the python programm when an error occurs
  def print(self, text, error=False, warning=False, debug=False, attention=False):
    temp = __printHelper__(text, error, warning, debug, attention)
    if self.benchliveLog == None:
      BLPrint("Called BLPrinter.print before setting benchliveLog", error=True)
    with open(self.benchliveLog, "a") as f:
      f.write(temp)
    if not self.muteBLOutput:
      sys.stdout.write(temp)
    if error:
      raise BenchLiveError()
