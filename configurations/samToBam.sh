#!/bin/bash
fastq_file=$1
reference_file=$2
run_dir=$3
logfile=$4

errorOccured=0
samtools view -bS ${run_dir}/samFiles/finalSamFile.sam > ${run_dir}/samFiles/finalSamFile.bam
[[ $? = 1 ]] && errorOccured=1

[[ $errorOccured = 0 ]] && echo 'PASSED'
