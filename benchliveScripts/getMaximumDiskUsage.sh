#!/bin/bash

pid=$2

TEMPMAX=0
while kill -0 $pid 2> /dev/null; do
    # Do stuff
    CURRENTDU=$(du -sc $1 | tail -n 1 | awk '{print $1}')
    [[ -z $CURRENTDU ]] || if [ "$CURRENTDU" -gt "$TEMPMAX" ]; then TEMPMAX=$CURRENTDU; fi
    sleep 10
done
CURRENTDU=$(du -sc $1 | tail -n 1 | awk '{print $1}')
[[ -z $CURRENTDU ]] || if [ "$CURRENTDU" -gt "$TEMPMAX" ]; then TEMPMAX=$CURRENTDU; fi
echo ${TEMPMAX}KiB > "$3"
