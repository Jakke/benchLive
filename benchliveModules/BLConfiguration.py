#!/usr/bin/python3

import sys
import os
import re
import time
from benchliveModules.BLPrinter import *

class BLConfiguration(object):
  """ A class for storing all information necessary to make a HiLive Run """

  def __init__(self, configFile):
    # mandatory settings
    # TODO list as comment
    # runtime settings (those are filled at runtime)
    # TODO list as comment

    self.setting_exePrefix = self.__getBLConfigPropertyFromFile("Executable prefix", configFile)
    self.setting_hiliveCall = self.__getBLConfigPropertyFromFile("HiLive call", configFile)
    self.setting_hiliveBuildCall = self.__getBLConfigPropertyFromFile("HiLive-build call", configFile)
    self.setting_isSeqanIndex = bool(int(self.__getBLConfigPropertyFromFile("(info) Index is seqan index", configFile)))
    self.setting_gapPattern = self.__getBLConfigPropertyFromFile("(info) Gap pattern", configFile)
    self.setting_readsTag = self.__getBLConfigPropertyFromFile("Reads Tag", configFile)
    self.setting_referenceTag = self.__getBLConfigPropertyFromFile("Reference Tag", configFile)
    self.setting_profileRuntime = bool(int(self.__getBLConfigPropertyFromFile("Profile runtime", configFile)))
    self.setting_runRABEMA = bool(int(self.__getBLConfigPropertyFromFile("Start RABEMA after HiLive finishes", configFile)))
    self.customScript = self.__getBLConfigPropertyFromFile("Custom validation bash script (optional)", configFile, stopOnError = False) # optional, defaults to ""
    self.title = self.__getBLConfigPropertyFromFile("Title (optional)", configFile, stopOnError = False)                  # optional, defaults to ""

    self.setupComputableSettings(self.setting_hiliveCall, configFile)

    if not os.path.exists(self.customScript):
      self.customScript = os.path.join(self.benchliveDir, self.customScript)
    if not os.path.exists(self.customScript):
      BLPrint("Could not find customScript", warning=True)
      self.customScript = ""


  # first helper function for __init__
  def __getBLConfigPropertyFromFile(self, name, filePath, stopOnError = True):
    res = ""
    with open(filePath) as f:
      lines = f.readlines()
      for line in lines:
        if line.startswith(name + ":"):
          res = line.split(name + ":")[1].strip()
    if res=="" and stopOnError:
      BLPrint("Could not parse property '" + name + "' from file '" + filePath + "'", error = True)
    return res


  def computeTiles(self, hiliveCall):
    tileArguments = re.findall("(?:-t|--tiles)\s*(?:\d+\s+)+", self.setting_hiliveCall) # list of arguments with -t flag (i.e. ["-t 1011 1012", "--tiles 1013"] )
    if tileArguments and tileArguments != []:
      tiles = re.findall("\d+", " ".join(tileArguments))
      return (tiles)
    else:
      return ["all"]


  # third helper function for __init__
  def setupComputableSettings(self, hiliveCall, configFile):
    self.configFile = configFile

    self.benchliveDir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    if self.title == "":
      self.title = os.path.basename(configFile.rstrip('/'))


    match = re.search("(?:-l|--lanes)\s*(\d+)", self.setting_hiliveCall)
    if match:
      self.lanes = match.group(1)
    else:
      self.lanes = "all"

    self.tiles = self.computeTiles(hiliveCall)
    self.basecallsDir = os.path.join(self.benchliveDir, "data", "samples", self.setting_readsTag, self.setting_readsTag + "_" + self.lanes + "L_" + "T".join(self.tiles) + "T", "BaseCalls")
    self.fastqFile = os.path.join(self.benchliveDir, "data", "samples", self.setting_readsTag, self.setting_readsTag + ".fastq")
    self.updatedFastqFile = os.path.join(os.path.dirname(self.basecallsDir), self.setting_readsTag + ".updatedQNAME.fastq")
    self.referenceFile = os.path.join(self.benchliveDir, "data", "references", self.setting_referenceTag, self.setting_referenceTag + ".fasta")


    match = re.search("(?:-t|--trim)\s*(\d+)", self.setting_hiliveBuildCall)
    if match:
      self.indexTrimming = match.group(1)
    else:
      self.indexTrimming = "not"

    if re.search("--do-not-convert-spaces",self.setting_hiliveBuildCall):
      self.indexIds_spaces2underscores = False
    else:
      self.indexIds_spaces2underscores = True

    if re.search("--trim-after-space",self.setting_hiliveBuildCall):
      self.indexIds_trimAfterFirstSpace = True
    else:
      self.indexIds_trimAfterFirstSpace = False

    suffix = ""
    if not self.setting_isSeqanIndex:
      suffix = ".kix"
    self.indexFile = os.path.join(self.benchliveDir, "data", "references", self.setting_referenceTag, self.setting_referenceTag + "_" + self.setting_gapPattern + "gapped" + "_" + self.indexTrimming + "Trimmed_ids" + str(int(not self.indexIds_spaces2underscores)) + str(int(self.indexIds_trimAfterFirstSpace)) + suffix)
    self.indexLogFile = self.indexFile + ".creationLog"
    temp_dir_name = self.setting_readsTag + "_" + self.lanes + "L_" + "T".join(self.tiles) + "T" + "_on_" + self.setting_referenceTag
    self.goldstandardPrefix = os.path.join(self.benchliveDir, "data", "goldstandards", temp_dir_name, temp_dir_name)


    with open(self.fastqFile, "r") as f:
      lineCounter = 0
      self.readLength = 0

      for line in f:
        if lineCounter == 4:
          lineCounter = 1
        else:
          lineCounter += 1

        if lineCounter == 2:
          self.readLength = max(self.readLength, len(line)-1)


  def setupRuntimeSettings(self, runDir):
    self.runDir = os.path.realpath(runDir)
    self.samOutputDir = os.path.join(self.runDir, "samFiles")
    self.finalSamFile = os.path.join(self.samOutputDir, "finalSamFile.sam")
    self.finalSamFile_rabemaCopy = os.path.join(self.samOutputDir, "finalSamFile_rabemaCopy.bam")
    self.alignmentFilesDir = os.path.join(self.runDir, "alignmentFiles")

    self.fastqToBclLogError = os.path.join(self.runDir, "fastq2bcl.errorLog")
    self.hiliveLogRegular = os.path.join(self.runDir, "hilive.regularLog")
    self.hiliveLogError = os.path.join(self.runDir, "hilive.errorLog")
    self.hiliveBuildLogRegular = os.path.join(self.runDir, "hiliveBuild.regularLog")
    self.hiliveBuildLogError = os.path.join(self.runDir, "hiliveBuild.errorLog")
    self.rabemaLogRegular = os.path.join(self.runDir, "rabema.regularLog")
    self.rabemaLogError = os.path.join(self.runDir, "rabema.errorLog")
    self.benchliveLog = os.path.join(self.runDir, "benchlive.log")
    self.customScriptLog = os.path.join(self.runDir, "customScript.log")
    self.diskUsageFile = os.path.join(self.runDir, "diskUsage.log")
    self.memoryUsageFile = os.path.join(self.runDir, "memoryUsage.log")
    self.indexRuntimeFile = os.path.join(self.runDir, "indexRuntime.log")
    self.indexMemoryUsageFile = os.path.join(self.runDir, "indexMemoryUsage.log")
    self.profilerStackFile = os.path.join(self.runDir, "profiler.regularLog")
    self.profilerErrorFile = os.path.join(self.runDir, "profiler.errorLog")
    self.profilerSvgFile = os.path.join(self.runDir, "runtimeProfile.svg")
    self.rabemaTempFile1 = self.goldstandardPrefix + "_temp1.sam"
    self.rabemaTempFile2 = self.goldstandardPrefix + "_temp2_postprocessed.sam"
    self.rabemaTempFile3 = self.goldstandardPrefix + "_temp3_postprocessed.bam"
    self.rabemaTempFile4 = self.goldstandardPrefix + "_temp4_postprocessed_sorted.bam"
    self.rabemaGoldstandardFile = self.goldstandardPrefix + ".gsi"
