BenchLive - Automated testing suite for HiLive
==============================================

BenchLive is a collection of tools designed to test and evaluate the reliability and performance of different HiLive versions.

Basic Structure
---------------

Each testrun of HiLive is described with a .BLConfig file. This file specifies the version of HiLive, the chosen set of parameters and all input files. The compiled executables of the different versions of HiLive, as well as all the input files are stored in the directory hierarchy of BenchLive. All output files, even intermediate files, are stored in one run folder which is generated when the test is started via `benchlive.py run`. In this folder there are all necessary log files.

When specified in the .BLConfig BenchLive creates a benchmark with SeqAn's RABEMA of the testrun. Several of these benchmarks can be combined via `benchlive.py compare` to generate a performance plot.

```
benchlive
├── benchlive.py
├── README.md
├── benchliveModules
│   └── ...
├── benchliveScripts
│   └── ...
├── comparisons
│   └── ...
├── configurations
│   ├── gettingStarted1.BLConfig
│   ├── gettingStarted2.BLConfig
│   ├── joinSamFiles.sh
│   ├── joinSamFiles_unpaired.sh
│   └── ...
├── data
│   ├── goldstandards
│   │   ├── gettingStarted_1L_1101T_on_gettingStarted
│   │   └── ...
│   ├── references
│   │   ├── gettingStarted
│   │   └── ...
│   └── samples
│       ├── gettingStarted
│       └── ...
├── hiliveExecutables
│   ├── master_hilive
│   ├── master_hilive-build
│   └── ...
└── runs
    ├── Y2017M03D22_16.58.02_gettingStarted
    └── ...
```


Installation
------------

BenchLive itself requires no installation, as it is only a collection of python scripts. However the following dependencies must be installed and added to the PATH variable:

 * gdb (should be installed by default)
 * samtools (version > 1.3.1)

Furthermore, BenchLive requires razers3 and a modified version of RABEMA from the SeqAn library. Assuming you want to install SeqAn in `/home/johndoe/seqan/` and have cloned BenchLive to `/home/johndoe/benchlive/`:
```
cd /home/johndoe
git clone https://github.com/seqan/seqan.git seqan
mkdir seqan/build
mkdir seqan/build/release
cd seqan/build/release
git apply /home/johndoe/benchlive/benchliveScripts/rabemaAbsRatePatch.patch
cmake ../.. && make rabema_prepare_sam rabema_evaluate rabema_build_gold_standard razers3
```

Also do not forget to add `/home/johndoe/seqan/build/release/bin/` to your PATH variable.


Checking setup
--------------

To check your BenchLive setup, run: (Assuming the build directory of your local HiLive repository is `/home/johndoe/hilive/build/` )
```
benchlive.py store master /home/johndoe/hilive/build "make"
benchlive.py run configurations/gettingStarted*
benchlive.py compare runs/*gettingStarted
```
Note, that with complex HiLive repositories it might be easier to compile, copy and rename the executables by hand instead of using `benchlive.py store`.

Creating a new test configuration
---------------------------------

A .BLConfig file looks like: (without any line breaks, it is just hard to show this here)
```
Title (optional):                           gettingStarted
Executable prefix:                          master
HiLive call:                                <hilive> --temp <TempDir> -n 1 -l 1 -t 1101 <BaseCallsDir> <Index> <MaxReadLength> <OutputDir>
HiLive-build call:                          <hilive-build> --do-not-convert-spaces --trim-after-space -o <Index> <Reference>
(info) Index is seqan index:                0
(info) Gap pattern:                         111111111111111
Reads Tag:                                  gettingStarted
Reference Tag:                              gettingStarted
Profile runtime:                            0
Start RABEMA after HiLive finishes:         1
Custom validation bash script (optional):   configurations/joinSamFiles_unpaired.sh
```

Placeholders like `<hilive>`, `<hilive-build>`, `<Index>` and `<MaxReadLength>` get automatically substituted by corresponding paths or values. Note, that you can use `<OutputDir>` as well as `<OutputFile>`. The arguments `--do-not-convert-spaces --trim-after-space` for `hilive-build` are necessary for running RABEMA. The validation script is launched after HiLive finished and returns `PASSED` on success. Note also that all lines with (info) must contain valid values in respect to the chosen executable prefix. They are necessary because BenchLive cannot parse that information alone.

If you want to add a new reference genome or a new read sample just copy the file in a respective folder in `data/` and rename it like this folder. Unfortunately, only the file extensions .fasta and .fastq are allowed (.fa and .fq are NOT). Assuming, the original files are called `ref.fa` and `reads.fq`:

```
mkdir /home/johndoe/benchlive/data/references/myReference/
mkdir /home/johndoe/benchlive/data/samples/shortDummyReads/
cp ref.fa /home/johndoe/benchlive/data/references/myReference/myReference.fasta
cp reads.fq /home/johndoe/benchlive/data/references/shortDummyReads/shortDummyReads.fastq
```

The `data/` folder is never checked for integrity, so you should not modify its content. However most files in it are checked for existence, so deleting them entirely is usually safe, because all files except reference .fasta and read sample .fastq can and will be computed automatically.


Development TODO notes
----------------------
uses results from flamegraph and poor mans profiler
confusing all_tiles() when there is only one present. (assumes 96 instead of checking how many are present)
automated multilane basecalls generation is not supported
