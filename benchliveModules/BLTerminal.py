#!/usr/bin/python3

import os
import time
from benchliveModules.BLPrinter import *
from threading import Thread
from queue import Queue, Empty
from subprocess import Popen, PIPE

class NonBlockingStreamReader:
  """ implementation taken from http://eyalarubas.com/python-subproc-nonblock.html """

  def __init__(self, stream):
    ''' stream: the stream to read from. Usually a process' stdout or stderr. '''
    self._s = stream
    self._q = Queue()

    def _populateQueue(stream, queue):
      ''' Collect lines from 'stream' and put them in 'quque'. '''
      while True:
        line = stream.readline()
        if line:
          queue.put(line)
        else:
          raise UnexpectedEndOfStream

    self._t = Thread(target = _populateQueue, args = (self._s, self._q))
    self._t.daemon = True
    self._t.start() #start collecting lines from the stream

  def readline(self, timeout = None):
    try:
      return self._q.get(block = timeout is not None, timeout = timeout)
    except Empty:
      return None

class UnexpectedEndOfStream(Exception):
  """ implementation taken from http://eyalarubas.com/python-subproc-nonblock.html """
  pass


class BLTerminal(object):
  """ A class emulating a terminal """

  def __init__(self):
    self.process = Popen("bash", stdin=PIPE, stdout=PIPE, stderr=PIPE)
    self.nbsr = NonBlockingStreamReader(self.process.stdout)
    self.nbsr_error = NonBlockingStreamReader(self.process.stderr)


  def sendCommand(self, command, returnOutput = False, discardOutput = False, wait = True):
    if discardOutput:
      wait = True
    if returnOutput:
      self.discardOutput()
      wait = True
    if wait:
      output, err = Popen(['pgrep', '-P', str(self.process.pid)], stdout=PIPE).communicate()
      oldpidlist = output.decode("utf-8").strip().split()

    command += "\n"
    self.process.stdin.write(bytes(command, 'UTF-8'))
    self.process.stdin.flush()

    time.sleep(0.1)
    if wait:
      while self.isBusy(basePidSet = set(oldpidlist)):
        time.sleep(1)
    if discardOutput:
      self.discardOutput()
    if returnOutput:
      return self.getOutput()


  def getOutput(self):
    callOutput = ""
    errOutput = ""
    while True:
        outputLine = self.nbsr.readline(timeout=0.1)
        if not outputLine:
            break
        callOutput += outputLine.decode("utf-8")
    while True:
        outputLine = self.nbsr_error.readline(timeout=0.1)
        if not outputLine:
            break
        errOutput += outputLine.decode("utf-8")
    return (callOutput, errOutput)


  def discardOutput(self):
    _ = self.getOutput()


  def isBusy(self, basePidSet = set()):
    output, err = Popen(['pgrep', '-P', str(self.process.pid)], stdout=PIPE).communicate()
    pidlist = output.decode("utf-8").strip().split()
    if (set(pidlist) - basePidSet == set()): # if no (new) childs of self.process exist
      return False
    return True


  def wait4all(self):
    while self.isBusy():
      time.sleep(1)
