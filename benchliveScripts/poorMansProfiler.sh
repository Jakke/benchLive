#!/bin/bash
sleeptime=5
pid=$1

echo "" > $2
while kill -0 $pid 2> /dev/null; do
    gdb -ex "set pagination 0" -ex "thread apply all bt" -batch -p $pid >> $2 2>$3
    sleep $sleeptime
  done
