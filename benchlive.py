#!/usr/bin/python3

import sys
import traceback
import argparse
import time
import glob
from benchliveModules.BLConfiguration import *
from benchliveModules.BLInstance import *
from benchliveModules.BLComparison import *


global_description_store= 'Compile a specified HiLive Commit and store the executables'
global_description_run = 'Run a set of BenchLive configurations'
global_description_resume = 'Resume a set of unfinished BenchLive directories'
global_description_compare = 'Compare the results of several run directories and create overview'
global_usage_message ='''\x1b[1;33mbenchlive.py\x1b[0m <command> [<args>]

Available commands are:
 \x1b[1;33mstore\x1b[0m                       ''' + global_description_store + '''
 \x1b[1;33mrun\x1b[0m                         ''' + global_description_run + '''
 \x1b[1;33mresume\x1b[0m                      ''' + global_description_resume + '''
 \x1b[1;33mcompare\x1b[0m                     ''' + global_description_compare + '''

To get help for a specific command run
 \x1b[1;33mbenchlive.py\x1b[0m <command> --help
'''


class BenchLive(object):
  ''' Some ideas come from http://chase-seibert.github.io/blog/2014/03/21/python-multilevel-argparse.html '''

  def __init__(self):
    parser = argparse.ArgumentParser(usage=global_usage_message)
    parser.add_argument('command', help=argparse.SUPPRESS, nargs='?')

    # parse_args defaults to [1:] for args, but you need to
    # exclude the rest of the args too, or validation will fail
    args = parser.parse_args(sys.argv[1:2])
    if args.command==None:
      parser.print_help()
      exit(0)

    if not hasattr(self, args.command.replace("-","_")):
      BLPrint ('Unrecognized command: '+args.command, error=True) # any error message via BLPrint() exits

    # use dispatch pattern to invoke method with same name
    getattr(self, args.command.replace("-","_"))()


  def store(self):
    parser = argparse.ArgumentParser(description= global_description_store,
                                     usage='''\x1b[1;33mbenchlive.py store\x1b[0m <commit> <hilive-build-dir> <hilive-build-call> [<git-checkout-dir]''')
    parser.add_argument('commit', help="commit as sha-1 hash")
    parser.add_argument('hilive_build_dir', metavar = "hilive-build-dir", help="Directory where to build from")
    parser.add_argument('hilive_build_call', metavar = "hilive-build-call", help="Building command (i.e. 'cmake .. && make')")
    parser.add_argument('git_checkout_dir', metavar = "git-checkout-dir", nargs='?', help="(optional) Directory where to checkout commit from, if checking out from hilive-build-dir is impossible.")
    args = parser.parse_args(sys.argv[2:])
    if args.git_checkout_dir==None:
      args.git_checkout_dir = args.hilive_build_dir

    h = BLExecutableHandler(args.hilive_build_dir, args.hilive_build_call, args.git_checkout_dir)
    h.storeCommit(args.commit)


  def __stepHilive(self, i):
    i.guaranteeIndexExistence()
    i.guaranteeBaseCallDirExistence()
    i.startHiLive()
    i.startMemoryLogger()
    i.startDiskSpaceLogger()
    if i.configuration.setting_profileRuntime:
      i.startRuntimeProfiler()
    i.waitUntilHiliveFinishes()
    i.finalizeHiLiveRun()
    i.saveToFile(os.path.join(i.configuration.runDir, "benchliveInstanceSave"))


  def __stepScript(self, i):
    i.runCustomScript()
    i.saveToFile(os.path.join(i.configuration.runDir, "benchliveInstanceSave"))


  def __stepRabema(self, i):
    i.guaranteeGoldstandardExistence()
    i.startRabema()
    i.saveToFile(os.path.join(i.configuration.runDir, "benchliveInstanceSave"))


  def __printNotification(self, message, configFileName, verbose, last = False):
    prefix = os.path.basename(configFileName)
    if verbose:
      BLPrint(prefix+'\t... ' + message)
    else:
      if last:
        print(message)
      else:
        sys.stdout.write(message + "\t... ")
        sys.stdout.flush()

  def __printStartNotification(self, configFileName, verbose):
    prefix = os.path.basename(configFileName)
    if not verbose:
      sys.stdout.write(time.strftime("[%d/%m/%Y %H:%M.%S]\t")+prefix+'\t... ')
      sys.stdout.flush()
    else:
      BLPrint(prefix+'\t... (started)')

  def __printHiLiveNotification(self, i, configFileName, verbose):
    END   = '\x1b[0m'
    ERROR = '\x1b[1;31m'
    GREEN = '\x1b[1;32m'
    if i.hilivePassed():
      message = GREEN+'HiLive PASSED'+END
    else:
      message = ERROR+'HiLive FAILED'+END
    self.__printNotification(message, configFileName, verbose)


  def __printScriptNotification(self, i, configFileName, verbose):
    END   = '\x1b[0m'
    ERROR = '\x1b[1;31m'
    GREEN = '\x1b[1;32m'
    if i.scriptPassed():
      message = GREEN+'Script PASSED'+END
    else:
      message = ERROR+'Script FAILED'+END
    self.__printNotification(message, configFileName, verbose)


  def __printRabemaNotification(self, i, configFileName, verbose):
    END   = '\x1b[0m'
    ERROR = '\x1b[1;31m'
    GREEN = '\x1b[1;32m'
    if not i.configuration.setting_runRABEMA:
      message = 'RABEMA not started'
    elif i.rabemaPassed():
      message = GREEN+'RABEMA PASSED'+END
    else:
      message = ERROR+'RABEMA FAILED'+END
    self.__printNotification(message, configFileName, verbose, last = True)


  def run(self):
    parser = argparse.ArgumentParser(description= global_description_run)
    parser.add_argument('paths', help = 'file paths of the configurations to run (wildcards like * are allowed)', nargs = '+')
    parser.add_argument('-x', '--exclude', help = 'files to exclude from <path>', nargs = '+')
    parser.add_argument('-v', '--verbose', help = 'show all program output', action = "store_true")
    args = parser.parse_args(sys.argv[2:])

    filenames = list()
    for entry in args.paths:
      for configFileName in glob.glob(entry):
        filenames.append(configFileName)

    if args.exclude:
      for entry in args.exclude:
        for configFileName in glob.glob(entry):
          filenames.remove(configFileName)

    for configFileName in filenames:
      try:
        self.__printStartNotification(configFileName, args.verbose)
        c = BLConfiguration(configFile = configFileName)
        i = BLInstance(configuration = c, muteBLOutput = not args.verbose)
        i.setupRunDir()
        i.saveToFile(os.path.join(i.configuration.runDir, "benchliveInstanceSave"))

        self.__stepHilive(i)
        self.__printHiLiveNotification(i, configFileName, args.verbose)

        if i.hilivePassed():
          self.__stepScript(i)
        self.__printScriptNotification(i, configFileName, args.verbose)

        if i.configuration.setting_runRABEMA and i.scriptPassed():
          self.__stepRabema(i)
        self.__printRabemaNotification(i, configFileName, args.verbose)
      except KeyboardInterrupt:
        BLPrint("")
        BLPrint("caught a keyboard interrupt.", error = True)
      except BenchLiveError:
        self.__printNotification("ERROR, see BenchLive log.", configFileName, args.verbose, last = True)
      except:
        self.__printNotification("Unexpected error.", configFileName, args.verbose, last = True)
        traceback.print_exc(file=sys.stdout)

    BLPrint("done.")


  def resume(self):
    parser = argparse.ArgumentParser(description= global_description_compare)
    parser.add_argument('paths', help = 'file paths of the run directories to resume (wildcards like * are allowed)', nargs = '*')
    parser.add_argument('-x', '--exclude', help = 'directories to exclude from <path>', nargs = '+')
    parser.add_argument('-v', '--verbose', help = 'show all program output', action = "store_true")
    parser.add_argument('--startWithScript', help = 'Jump directly to postprocessing script execution', action = "store_true")
    parser.add_argument('--startWithRabema', help = 'Jump directly to RABEMA execution', action = "store_true")
    parser.add_argument('--statOnly', help = 'print only status', action = "store_true")
    args = parser.parse_args(sys.argv[2:])
    dirnames = list()
    for entry in args.paths:
      for dirname in glob.glob(entry):
        dirnames.append(dirname)

    if args.exclude:
      for entry in args.exclude:
        for dirname in glob.glob(entry):
          dirnames.remove(dirname)

    if dirnames == list():
      BLPrint("No run directories specified", error = True)

    for d in dirnames:
      if not os.path.exists(os.path.join(d, "benchliveInstanceSave")):
        BLPrint("Found no file named 'benchliveInstanceSave' in " + d, error = True)

    for dirname in dirnames:
      try:
        configFileName = os.path.basename(dirname)[21:]
        self.__printStartNotification(configFileName, args.verbose)
        i = BLInstance(None, muteBLOutput = not args.verbose)
        i.loadFromFile(os.path.join(dirname, "benchliveInstanceSave"))

        if not i.hilivePassed() and not args.statOnly:
          self.__stepHilive(i)
        self.__printHiLiveNotification(i, configFileName, args.verbose)

        if (not i.scriptPassed() or args.startWithScript) and not args.statOnly:
          self.__stepScript(i)
        self.__printScriptNotification(i, configFileName, args.verbose)

        if i.configuration.setting_runRABEMA and (args.startWithRabema or not i.rabemaPassed()) and not args.statOnly:
          self.__stepRabema(i)
        self.__printRabemaNotification(i, configFileName, args.verbose)
      except KeyboardInterrupt:
        BLPrint("")
        BLPrint("caught a keyboard interrupt.", error = True)
      except BenchLiveError:
        self.__printNotification("ERROR, see BenchLive log.", configFileName, args.verbose, last = True)
      except:
        self.__printNotification("Unexpected error.", configFileName, args.verbose, last = True)
        traceback.print_exc(file=sys.stdout)
    BLPrint("done.")


  def compare(self):
    parser = argparse.ArgumentParser(description= global_description_compare)
    parser.add_argument('paths', help = 'file paths of the run directories to compare (wildcards like * are allowed)', nargs = '*')
    outputDefault = os.path.join(os.path.dirname(os.path.realpath(__file__)), "comparisons", time.strftime("Y%YM%mD%d_%H.%M.%S"))
    parser.add_argument('-x', '--exclude', help = 'directories to exclude from <path>', nargs = '+')
    parser.add_argument('-a', '--allAfter', help = 'Compare all directories in <BenchLive directory>/runs which were created at or after the specified time. Time is formatted like '+time.strftime("Y%YM%mD%d_%H.%M.%S"))
    parser.add_argument('-o', '--outputFile', default = outputDefault , help = 'Path to output file (without file extension)')
    # parser.add_argument('-v', '--verbose', help = 'show all program output', action = "store_true")
    args = parser.parse_args(sys.argv[2:])

    dirnames = list()
    for entry in args.paths:
      for dirname in glob.glob(entry):
        dirnames.append(dirname)

    if args.exclude:
      for entry in args.exclude:
        for dirname in glob.glob(entry):
          dirnames.remove(dirname)

    if args.allAfter:
      timeFormatPattern = "^Y\d\d\d\dM\d\dD\d\d_\d\d\.\d\d\.\d\d"
      run_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "runs")
      for elem in os.listdir(run_dir):
        if re.match(timeFormatPattern, elem) and elem >= args.allAfter:
          dirnames.append(os.path.join(run_dir, elem))

    if dirnames == list():
      BLPrint("No run directories specified", error = True)

    for d in dirnames:
      if not os.path.exists(os.path.join(d, "benchliveInstanceSave")):
        BLPrint("Found no file named 'benchliveInstanceSave' in " + d, error = True)

    comparison = BLComparison()
    for dirname in dirnames:
      i = BLInstance(None)
      i.loadFromFile(os.path.join(dirname, "benchliveInstanceSave"))
      comparison.addRun(i)
    comparison.createGraphic(args.outputFile)


if __name__ == '__main__':
  BenchLive()
