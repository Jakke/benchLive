#!/bin/bash

pid=$1
#echo "process id =" $pid

TEMPMAXMEM=0
while kill -0 $pid 2> /dev/null; do
    # Do stuff
    CURRENTMEM=$(ps -p $pid -o rss --no-headers)
    [[ -z $CURRENTMEM ]] || if [ "$CURRENTMEM" -gt "$TEMPMAXMEM" ]; then TEMPMAXMEM=$CURRENTMEM; fi
    sleep 0.25
done
echo ${TEMPMAXMEM}KiB > "$2"
